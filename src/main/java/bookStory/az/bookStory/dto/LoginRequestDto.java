package bookStory.az.bookStory.dto;

import bookStory.az.bookStory.enums.UserType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LoginRequestDto {
    String email;
    String password;
    boolean isActive;
    @Enumerated(EnumType.STRING)
    UserType userType;
}
