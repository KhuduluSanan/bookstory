package bookStory.az.bookStory.dto;

import bookStory.az.bookStory.enums.UserType;
import io.micrometer.common.lang.NonNull;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@NotEmpty
public class RegistrRequestDto {

    String surname;
    String email;
    String name;
    String password;
    boolean isActive;
    @Enumerated(EnumType.STRING)
    UserType userType;

}
