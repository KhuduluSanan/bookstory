package bookStory.az.bookStory.repository;

import bookStory.az.bookStory.entity.Book;
import bookStory.az.bookStory.entity.User;
import bookStory.az.bookStory.enums.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    boolean existsBookByBookName(String bookName);

    Optional<Book> findByBookNameIgnoreCase(String bookName);
}
