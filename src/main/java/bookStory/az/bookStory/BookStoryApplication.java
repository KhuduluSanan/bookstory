package bookStory.az.bookStory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookStoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookStoryApplication.class, args);
	}

}
