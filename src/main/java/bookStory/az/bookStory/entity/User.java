package bookStory.az.bookStory.entity;

import bookStory.az.bookStory.enums.UserType;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    String surname;
    String email;
    String password;
    @Enumerated(EnumType.STRING)
    UserType userType;
    boolean isActive;
    @OneToMany(mappedBy = "author")
    List<Book> books;
}
