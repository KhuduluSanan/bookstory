package bookStory.az.bookStory.service;

import bookStory.az.bookStory.dto.BookRequestDto;
import bookStory.az.bookStory.dto.LoginRequestDto;
import bookStory.az.bookStory.dto.RegistrRequestDto;

public interface BookService {

    String saveBook(BookRequestDto bookRequestDto);

    String getNameBook(String book);

    String deleteBook(Long id);
}
