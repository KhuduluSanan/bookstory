package bookStory.az.bookStory.service;

import bookStory.az.bookStory.dto.BookRequestDto;
import bookStory.az.bookStory.dto.LoginRequestDto;
import bookStory.az.bookStory.dto.RegistrRequestDto;
import bookStory.az.bookStory.entity.Book;
import bookStory.az.bookStory.entity.User;
import bookStory.az.bookStory.enums.UserType;

import java.util.List;

public interface UserService {
    Long registerUser(RegistrRequestDto registrRequestDto);

    String loginUser(LoginRequestDto loginRequestDto);


    User addBook(BookRequestDto bookRequestDto, String email);

    List<Book> getAuthorBooks(String email);

}
