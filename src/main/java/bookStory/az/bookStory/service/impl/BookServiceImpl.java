package bookStory.az.bookStory.service.impl;

import bookStory.az.bookStory.dto.BookRequestDto;
import bookStory.az.bookStory.entity.Book;
import bookStory.az.bookStory.entity.User;
import bookStory.az.bookStory.enums.UserType;
import bookStory.az.bookStory.repository.BookRepository;
import bookStory.az.bookStory.service.BookService;
import bookStory.az.bookStory.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final UserService userService;


    @Override
    public String saveBook(BookRequestDto bookRequestDto) {
        User user = userService.addBook(bookRequestDto, bookRequestDto.getEmail());
        boolean bookHave = bookRepository.existsBookByBookName(bookRequestDto.getBookName());
        Book book1 = Book.builder()
                .author(User.builder()
                        .id(user.getId())
                        .build())
                .bookName(bookRequestDto.getBookName())
                .build();
        if (user.getUserType() == UserType.AUTHOR && user.isActive() && !bookHave) {
            bookRepository.save(book1);
            return "Yüklənmə uğurla tamamlandı";
        }

        return "Yüklənmə uğursuz oldu - 1)Eyni adda Kitablar yüklənə bilməz 2)Yalnız Admin yükləmə edə bilər.";
    }

    @Override
    public String getNameBook(String book) {
        Optional<Book> bookOptional = bookRepository.findByBookNameIgnoreCase(book);

        return bookOptional.orElseThrow(() ->
                new RuntimeException("BU adla kitab yoxdur")).getBookName();
    }

    @Override
    public String deleteBook(Long id) {
        Optional<Book> byId = bookRepository.findById(id);
        if (byId.isPresent()) {
            bookRepository.deleteById(id);
            return "Kitab silindi - ID nömrəsi : " + id;
        }
        return "Bu ID-də kitab yoxdur " + id;
    }
}
