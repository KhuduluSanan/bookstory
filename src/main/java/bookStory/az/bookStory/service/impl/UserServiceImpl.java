package bookStory.az.bookStory.service.impl;

import bookStory.az.bookStory.config.AppConfigration;
import bookStory.az.bookStory.dto.BookRequestDto;
import bookStory.az.bookStory.dto.LoginRequestDto;
import bookStory.az.bookStory.dto.RegistrRequestDto;
import bookStory.az.bookStory.entity.Book;
import bookStory.az.bookStory.entity.User;
import bookStory.az.bookStory.enums.UserType;
import bookStory.az.bookStory.repository.UserRepository;
import bookStory.az.bookStory.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final AppConfigration appConfigration;
    private final UserRepository userRepository;

    @Override
    public Long registerUser(RegistrRequestDto registrRequestDto) {
        User mapUser = appConfigration.getMapper().map(registrRequestDto, User.class);
        return userRepository.save(mapUser).getId();
    }

    @Override
    public String loginUser(LoginRequestDto loginRequestDto) {
        User user = userRepository.findByEmail(loginRequestDto.getEmail());

        if (user != null && user.getPassword().equals(loginRequestDto.getPassword())) {
            user.setActive(true);
            userRepository.save(user);

            loginRequestDto.setActive(user.isActive());
            loginRequestDto.setUserType(user.getUserType());

            return "Login you ";

        }
        return "You have not account - Please register ! ";
    }

    @Override
    public User addBook(BookRequestDto bookRequestDto, String email) {
        User byEmail = userRepository.findByEmail(email);
        if (byEmail == null || !byEmail.isActive() || byEmail.getUserType() != UserType.AUTHOR) {
            throw new RuntimeException("Siz Admin deyilsiz");
        }
        return byEmail;
    }

    @Override
    public List<Book> getAuthorBooks(String email) {
        User byEmailAuthor = userRepository.findByEmail(email);
        if (byEmailAuthor == null || !byEmailAuthor.isActive() || byEmailAuthor.getUserType() != UserType.AUTHOR) {
            throw new RuntimeException("Belə email yoxdur databasdə");
        }
        return byEmailAuthor.getBooks();
    }
}
