package bookStory.az.bookStory.controller;

import bookStory.az.bookStory.dto.BookRequestDto;
import bookStory.az.bookStory.entity.Book;
import bookStory.az.bookStory.entity.User;
import bookStory.az.bookStory.service.BookService;
import bookStory.az.bookStory.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/books")
public class BookController {
    private final BookService bookService;
    private final UserService userService;

    @PostMapping
    public String addBook(@RequestBody BookRequestDto bookRequestDto, @RequestParam String email) {
        return bookService.saveBook(bookRequestDto);
    }

    @GetMapping("/author")
    public List<Book> getAuthorBooks(@RequestParam String email){
        return userService.getAuthorBooks(email);
    }

    @GetMapping("/{book}")
    public String getNameBook(@PathVariable String book){
        return bookService.getNameBook(book);
    }

    @DeleteMapping("/{id}")
    public String deleteBook (@PathVariable Long id){
        return bookService.deleteBook(id);
    }
}

