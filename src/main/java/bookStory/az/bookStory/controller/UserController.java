package bookStory.az.bookStory.controller;

import bookStory.az.bookStory.dto.LoginRequestDto;
import bookStory.az.bookStory.dto.RegistrRequestDto;
import bookStory.az.bookStory.entity.Book;
import bookStory.az.bookStory.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    @PostMapping("/register")
    public Long registerUser(@RequestBody RegistrRequestDto registrRequestDto) {
      return userService.registerUser(registrRequestDto);
    }

    @PostMapping("/login")
    public String loginUser(@RequestBody LoginRequestDto loginRequestDto){
        return userService.loginUser(loginRequestDto);
    }


}
